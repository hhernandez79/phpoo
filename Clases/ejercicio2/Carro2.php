<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;

	//Se agrega propiedad año de fabricación para agregar la nueva funcionalidad
	public $fabricacion;
	//public $resultado;

	//declaracion del método verificación
	public function verificacion($fabricacion){
		
		//Condición para verificar que existen datos en los campos del formulario
		if(!empty($_POST)){

			//Fechas para realizar la comparación 
			$pre = "1990-01";
			$post = "2010-01";

			//Uso de funcion strtotime para dar formato a las fechas y realizar su comparación
			$fabricacionFormato = strtotime($this->$fabricacion);
			$preFormato = strtotime($pre);
			$postFormato = strtotime($post);


			if($fabricacionFormato < $preFormato){
				return "No";
				
			}

			else if(($fabricacionFormato > $preFormato) && ($fabricacionFormato < $postFormato) ){
				return "Revisión";
			}

			else if($fabricacionFormato > $postFormato){
				return "Si";
				
			}

		}
		
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();


//Asignacion de las propiedades de la clase a partir del valor de los campos
if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->fabricacion=$_POST['fabricacion'];

	//$mensaje=$Carro1->verificacion($_POST['fabricacion']); 
	//$resultado=$Carro1->verificacion($_POST['fabricacion']); 

	//date("Y-m", $_POST['fabricacion']);
	//$Carro1->resultado = $resultado;

	//$Carro1->resultado = verificacion($_POST['fabricacion']);
}




