<?php

include_once('transporte.php');

//declaracion de la subclase carro
class carro extends transporte{

    private $numero_puertas;

    //declaracion del constructor
    public function __construct($nom,$vel,$com,$pue){
        //sobreescritura de constructor de la clase padre
        parent::__construct($nom,$vel,$com);
        $this->numero_puertas=$pue;
            
    }

    // declaracion de metodo
    public function resumenCarro(){
        // sobreescribitura de metodo crear_ficha en la clse padre
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Numero de puertas:</td>
                    <td>'. $this->numero_puertas.'</td>				
                </tr>';
        return $mensaje;
    }
} 

$mensaje='';


if (!empty($_POST)){
	//declaracion de condicional para la opcion terrestre
	    if (($_POST['tipo_transporte'])== 'terrestre') {
			//creacion del objeto con sus respectivos parametros para el constructor
			$carro1= new carro('carro','200','gasolina','4');
            $mensaje=$carro1->resumenCarro();
			
	}

}



?>