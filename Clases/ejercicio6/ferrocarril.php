<?php

include_once('transporte.php');

//declaracion de la nueva subclase ferrocarril
class ferrocarril extends transporte{

    private $numero_vagones;

    //declaracion del constructor
    public function __construct($nom,$vel,$com,$vagones){
        //sobreescritura de constructor de la clase padre
        parent::__construct($nom,$vel,$com);
        $this->numero_vagones=$vagones;
            
    }

    // declaracion de metodo
    public function resumenFerrocarril(){
        // sobreescribitura de metodo crear_ficha en la clase padre
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Numero de vagones:</td>
                    <td>'. $this->numero_vagones.'</td>				
                </tr>';
        return $mensaje;
    }
} 

$mensaje='';


if (!empty($_POST)){
	//declaracion de condicional para la opcion ferrocarril
	    if (($_POST['tipo_transporte'])=='ferrocarril') {
			//creacion del objeto con sus respectivos parametros para el constructor
			$ferrocarril1= new ferrocarril('locomotora','350','diésel','8');
            $mensaje=$ferrocarril1->resumenFerrocarril();
			
	}

}



?>