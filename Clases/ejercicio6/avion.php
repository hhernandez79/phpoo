<?php

include_once('transporte.php');

// declaración de la subclase avión
class avion extends transporte{

    private $numero_turbinas;
    
    //sobreescritura de constructor
    public function __construct($nom,$vel,$com,$tur){
        parent::__construct($nom,$vel,$com);
        $this->numero_turbinas=$tur;
    }

    // sobreescritura de metodo ficha
    public function resumenAvion(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Numero de turbinas:</td>
                    <td>'. $this->numero_turbinas.'</td>				
                </tr>';
        return $mensaje;
    }
}

$mensaje='';


if (!empty($_POST)){
	//declaracion de condicional para la opcion terrestre
	    if (($_POST['tipo_transporte'])=='aereo') {
			//creacion del objeto con sus respectivos parametros para el constructor
			$jet1= new avion('jet','400','gasoleo','2');
			$mensaje=$jet1->resumenAvion();
			
	}

}


?>