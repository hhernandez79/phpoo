<?php  
//declaracion de clase token
	class tokenNuevo{
		//declaracion de atributos
		private $nombre;
		private $token;
	
		//declaracion de metodo constructor
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
			
			//Método para calcular el token de 4 letras mayusculas aleatorias
			$token = "";
			//Patron para la selección de las 4 letras mayusculas
			$patron = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$max = strlen($patron)-1;

				//Ciclo para la elección aleatoria de las 4 letras mayusculas
				for($i = 0; $i < 4; $i++){
				$this->token=$token .= substr($patron, mt_rand(0,$max), 1);
			}
		}

		//declaracion del metodo mostrar el mensaje con el nombre y token
		public function mostrar(){
			return 'Hola '.$this->nombre.' este es tu token: '.$this->token;
		}

		//declaracion de metodo destructor
		public function __destruct(){
			//destruye token
			$this->token='El token ha sido destruido';
			echo $this->token;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase tokenNuevo
	$token1= new tokenNuevo($_POST['nombre']);
	$mensaje=$token1->mostrar();
}


?>
